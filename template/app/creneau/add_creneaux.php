<h1> Ajout d'un créneau</h1>

<div class="formulaire">
    <form action="" method="post" novalidate class="wrapform">

        <div class="select">
            <?php echo $form->label('id_salle', 'Salle :'); ?>
            <?php echo $form->selectEntity('id_salle', $salle, 'title'); ?>
            <?php echo $form->error('id_salle'); ?>

        </div>

        <?php echo $form->label('start_at', 'Début :'); ?>
        <?php echo $form->input('start_at', 'date'); ?>
        <?php echo $form->error('start_at'); ?>

        <?php echo $form->label('hours', 'Nombre d\'heures :'); ?>
        <?php echo $form->input('hours', 'text'); ?>
        <?php echo $form->error('hours'); ?>

        <div class="btn">
            <?php echo $form->submit('submitted', 'Envoyer'); ?>
        </div>
    </form>
</div>