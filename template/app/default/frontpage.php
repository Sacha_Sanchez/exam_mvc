<h1>Données</h1>

<!-- Tableau User-->

<table>
    <thead>
    <tr>
        <th scope="col">Nom</th>
        <th scope="col">Email</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user) { ?>
        <tr>
            <td><?= $user->getNom();?></td>
            <td><?= $user->getEmail(); ?></td>
        </tr>
    <?php }?>
    </tbody>
</table>

<div class="link">
    <a href="<?= $view->path('add_users'); ?>">Ajouter un User</a>
</div>

<!--Tableau Salle-->

<table>
    <thead>
    <tr>
        <th scope="col">Nom de la salle</th>
        <th scope="col">Nombre de user</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($salles as $salle) { ?>
        <tr>
            <td><?= $salle->getTitle();?></td>
            <td><?= $salle->getMaxuser(); ?></td>
        </tr>
    <?php }?>
    </tbody>
</table>

<div class="link">
    <a href="<?= $view->path('add_salles'); ?>">Ajouter une Salle</a>
</div>

<!-- Tableau Créneau-->

<table>
    <thead>
    <tr>
        <th scope="col">Nom de la salle</th>
        <th scope="col">Début</th>
        <th scope="col">Nombre d'heures</th>
        <th scope="col">Détail</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($creneaux as $creneau) { ?>
        <tr>
            <td><?= $creneau->getIdSalle();?></td>
            <td><?= $creneau->getStartAt(); ?></td>
            <td><?= $creneau->getNbrehours(); ?></td>
        </tr>
    <?php }?>
    </tbody>
</table>

<div class="link">
    <a href="<?= $view->path('add_creneaux'); ?>">Ajouter un Créneau</a>
</div>