<?php

namespace App\Controller;

use App\Model\UsersModel;
use App\Service\Form;
use App\Service\Validation;

class UsersController extends BaseController
{
    public function add () {
        $errors = array ();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                UsersModel::insert($post);
                $this->addFlash('success', 'Votre formulaire a été envoyé avec succès!');
                $this->redirect('add_users');
            }
        }
        $form = new Form($errors);
        $this->render('app.users.add_users', array(
            'form'=>$form,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom',2, 100);
        $errors['email'] = $v->emailValid($post['email']);
        $emailexist =UsersModel::findByColumn('email',$post['email']);
        if($emailexist==$post['email'])
        {
            $errors['email']='Adresse E-mail déjà utilisé!';
        }
        return $errors;
    }
}