<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class SalleModel extends AbstractModel
{
    protected static $table = 'salle';

    protected $id;
    protected $title;
    protected $maxuser;

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (title, maxuser) VALUES (?,?)",
            array($post['title'], $post['maxuser'])
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getMaxuser()
    {
        return $this->maxuser;
    }

    /**
     * @param mixed $maxuser
     */
    public function setMaxuser($maxuser): void
    {
        $this->maxuser = $maxuser;
    }

}