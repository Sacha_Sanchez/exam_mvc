<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UsersModel extends AbstractModel
{
    protected static $table = 'user';

    protected $id;
    protected $nom;
    protected $email;


    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (nom, email) VALUES (?,?)",
            array($post['nom'], $post['email'])
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }
}