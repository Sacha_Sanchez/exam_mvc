<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;

class SalleController extends BaseController
{
    public function add () {
        $errors = array ();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                SalleModel::insert($post);
                $this->addFlash('success', 'Votre formulaire a été envoyé avec succès!');
                $this->redirect('add_salles');
            }
        }
        $form = new Form($errors);
        $this->render('app.salle.add_salle', array(
            'form'=>$form,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'title',2, 100);
        if(!empty($post['maxuser'])){
            if (!is_numeric($post['maxuser'])){
                $errors ['maxuser'] = 'Veuillez saisir uniquement des chiffres';
            }elseif(mb_strlen($post['maxuser']) > 11){
                $errors ['maxuser'] = 'Veuillez renseigner moins de 11 chiffres';
            }elseif ($post['maxuser'] < 0) {
                $errors['maxuser'] = 'Veuillez saisir un nombre positif';
            }
        }else{
            $errors ['maxuser'] = 'Veuillez renseigner ce champ';
        }
        return $errors;
    }
}