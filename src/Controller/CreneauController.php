<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;

class CreneauController extends BaseController
{
    public function add(){
        $errors = array ();
        $salle = SalleModel::all();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                CreneauModel::insert($post);
                $this->addFlash('success', 'Votre formulaire a été envoyé avec succès!');
                $this->redirect('add_creneaux');
            }
        }
        $creneau = CreneauModel::getRoomWithCreneau();
        $form = new Form($errors);
        $this->render('app.creneau.add_creneaux', array(
            'form'=>$form,
            'salle'=>$salle,
            'creneau'=>$creneau
        ));
    }

    public function single ($id){
        $this->render('app.creneau.single', array(

        ));
    }

    private function validate($v, $post)
    {
        $errors = [];
        if(!empty($post['hours'])){
            if (!is_numeric($post['hours'])){
                $errors ['hours'] = 'Veuillez saisir uniquement des chiffres entier';
            }elseif(mb_strlen($post['hours']) > 11){
                $errors ['hours'] = 'Veuillez renseigner moins de 11 chiffres';
            }elseif ($post['hours'] < 0) {
                $errors['hours'] = 'Veuillez saisir un nombre positif';
            }
        }else{
            $errors ['hours'] = 'Veuillez renseigner ce champ';
        }
        return $errors;
    }
}
