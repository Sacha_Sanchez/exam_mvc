<h1> Ajout d'un User</h1>

<div class="formulaire">
    <form action="" method="post" novalidate class="wrapform">
        <?php echo $form->label('nom', 'Nom :'); ?>
        <?php echo $form->input('nom', 'text'); ?>
        <?php echo $form->error('nom'); ?>

        <?php echo $form->label('email', 'Adresse-mail :'); ?>
        <?php echo $form->input('email', 'email'); ?>
        <?php echo $form->error('email'); ?>

        <div class="btn">
            <?php echo $form->submit('submitted', 'Envoyer'); ?>
        </div>
    </form>
</div>