<h1> Ajout d'une salle</h1>

<div class="formulaire">
    <form action="" method="post" novalidate class="wrapform">
        <?php echo $form->label('title', 'Titre :'); ?>
        <?php echo $form->input('title', 'text'); ?>
        <?php echo $form->error('title'); ?>

        <?php echo $form->label('maxuser', 'Nombre de personnes :'); ?>
        <?php echo $form->input('maxuser', 'text'); ?>
        <?php echo $form->error('maxuser'); ?>

        <div class="btn">
            <?php echo $form->submit('submitted', 'Envoyer'); ?>
        </div>
    </form>
</div>